from django.apps import AppConfig


class PagesJrConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'pages_jr'
